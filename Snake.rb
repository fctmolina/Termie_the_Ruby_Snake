require 'curses'
include Curses 
require_relative 'Link_List/LinkedList'

class Snake

	def initialize
		@win = Window.new(lines, cols, 0, 0)
		@title = "TERMIE THE RUBY SNAKE"
		@score = 0
		@level = (@score%10)+1
		@direction = :right
		@speed = 0.2
		@speed_incremented = false

		@snake_list = LinkedList.new
		@snake_list.add_element(lines/2,cols/2+1)
		@snake_list.add_element(lines/2,cols/2)
		@snake_list.add_element(lines/2,cols/2-1)
	end

	def setup_window(border_wall, border_roof)
		@win.box(border_wall,border_roof)

		@win.setpos(0,cols/2-@title.length/2)
		@win.addstr(@title)

		@win.setpos(0,cols/2+((cols/2)/2))
		@win.addstr("Level: ")
		@win.setpos(0,cols/2+((cols/2)/2) + 7)
		@win.addstr("" + @level.to_s)

		@win.setpos(0,cols/2+((cols/2)/2) + 15)
		@win.addstr("Score: ")
		@win.setpos(0,cols/2+((cols/2)/2) + 22)
		@win.addstr("" + @score.to_s)

		@win.setpos(0,0)
		@win.addstr("[q] quit [space] pause")
	end

	def refresh_window	
		@win.refresh
		@win.clear
	end

	def draw_snake
		snake_head = @snake_list.head
		while snake_head != nil
			@win.setpos(snake_head.xpos, snake_head.ypos)
			@win.addstr("s")
			snake_head = snake_head.next
		end
	end

	def change_direction?
		case getch
			when ?Q, ?q
				exit
			when ?W, ?w
				@direction = :up if @direction != :down
			when ?S, ?s 
				@direction = :down if @direction != :up
			when ?A, ?a 
				@direction = :left if @direction != :right
			when ?D, ?d 
				@direction = :right if @direction != :left
		end
	end

	def change_direction!
		case @direction
			when :up #then @snake_list.head.xpos -= 1
				current_node = @snake_list.head
				while current_node != nil
					current_node.xpos -= 1
					current_node = current_node.next
				end
			when :down #then @snake_list.head.xpos += 1
				current_node = @snake_list.head
				while current_node != nil
					current_node.xpos += 1
					current_node = current_node.next
				end
			when :left #then @snake_list.head.ypos -= 1
				current_node = @snake_list.head
				while current_node != nil
					current_node.ypos -= 1
					current_node = current_node.next
				end
			when :right #then @snake_list.head.ypos += 1
				current_node = @snake_list.head
				while current_node != nil
					current_node.ypos += 1
					current_node = current_node.next
				end
		end
	end	

	def set_speed
		if(@score % 10 == 0)
			if @speed_incremented == false
				@speed -= (@speed*0.10) unless @speed < 0.05
				@speed_incremented = true
			end
		else
			@speed_incremented = false
		end

		sleep( (@direction == :left or @direction == :right) ? @speed/2 : @speed )
	end	
end

init_screen
cbreak
noecho
stdscr.nodelay = 1
curs_set(0)
@snake = Snake.new

begin 
	loop do 
		@snake.setup_window("*","*")
		@snake.change_direction!
		@snake.draw_snake
		@snake.set_speed
		@snake.refresh_window
	end
ensure
	close_screen
end