class LinkedList
	attr_reader :head

	def initialize
		@head = nil
	end

	def add_element(xpos, ypos)
		new_node = Node.new(xpos, ypos)
		if @head.nil?
			@head = new_node
		else 
			current = @head
			while current.next != nil
				current = current.next
			end
			current.next = new_node
		end
	end
end

class Node
	attr_accessor :xpos, :ypos, :next

	def initialize(xpos, ypos)
		@xpos = xpos
		@ypos = ypos
	end
end